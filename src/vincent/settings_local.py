import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'vincent',
        'USER': 'vincent',
        'PASSWORD': 'vincent',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
MEDIA_ROOT = os.path.join(BASE_DIR, 'static/media/')
STATIC_ROOT = ''
COMPRESSOR_ROOT = os.path.join(BASE_DIR, 'static/compressor/')
COMPRESS_ENABLED = False
DEBUG = True