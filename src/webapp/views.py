from django.views.generic import TemplateView
from django.shortcuts import render
from django.conf import settings
from django.views import View
from django.urls import resolve
from django.db import connection


# Create your views here.

class BaseContextMixin(object):
    
    """ Base context mixin for base.html """

    og_meta = {
        'title': None,
    }

    def get_breadcrumbs(self):
        """ Returns breadcrubms as a list [{'name'=name, 'url'=url}, ... ] """
        i = 0
        breadcrumbs = []
        while(True):
            f = self.request.path[i:].find('/') + 1
            breadcrumb = self.request.path[0:f+i]
            try:
                url_entry = resolve(breadcrumb)
                breadcrumbs.append({
                    'url': breadcrumb,
                    'name': url_entry.view_name,
                })
            except:
                pass
            
            i = f + i
            if i == len(self.request.path): break
        return breadcrumbs[:-1]

    def get_site_settings(self):
        return { 'title': 'Internet center', }

    def get_context_data(self, **kwargs):
        """ Base context for base.html """
        context = super(BaseContextMixin, self)\
                        .get_context_data(**kwargs)

        context['site_settings'] = self.get_site_settings()
        context['breadcrumbs'] = self.get_breadcrumbs()
        context['connection'] = connection

        return context


class IndexView(BaseContextMixin, TemplateView):

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['plugins'] = settings.PLUGINS
        return context


class EmptyView(BaseContextMixin, TemplateView):
    
    template_name = "empty.html"
    
