# from django.contrib.auth.models import User, Group
from .models import Task
from rest_framework import serializers



class TaskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Task
        fields = ('name', 'description',)
