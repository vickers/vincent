from django import forms
from .models import Task
from django.db.models import Count
from django.utils.translation import ugettext_lazy as _
from django.utils import formats
from datetime import datetime


class DateTimeLocalInput(forms.DateTimeInput): 
    input_type = 'datetime-local'

    # def to_python(self, value):
    #     """Normalize data to a list of strings."""
    #     # Return an empty list if no input was given.
    #     if not value:
    #         return []
    #     return value.split(',')

    # def validate(self, value):
    #     """Check if value consists only of valid emails."""
    #     # Use the parent's handling of required fields, etc.
    #     import ipdb;ipdb.set_trace()
    #     super(DateTimeField, self).validate(value)


    # def format_value(self, value):
    #     import ipdb;ipdb.set_trace()
    #     return formats.localize_input(value, self.format or formats.get_format(self.format_key)[0])

class TaskModelChoiceField(forms.ModelChoiceField):

    def __init__(self, **kwargs):
        kwargs['queryset'] = Task.objects.annotate(tasks_count=Count('task'))\
                                        .order_by('-tasks_count')
        context = super(TaskModelChoiceField, self).__init__(**kwargs)

    def label_from_instance(self, obj):
        return '{} ({})'.format(obj.name, obj.tasks_count)



class TaskForm(forms.ModelForm):
    
    deadline = forms.DateTimeField(label=_('Deadline at'), widget=DateTimeLocalInput(), required=False)
    done_at =  forms.DateTimeField(label=_('Done at'), widget=DateTimeLocalInput(), initial=datetime.now(), required=False)
    done_at1 =  forms.DateTimeField(label=_('Done at1'), required=False)
    parent = TaskModelChoiceField()

    class Meta:
        model = Task
        fields = ['name', 'description', 'is_done', 'done_at', 'deadline', 'parent', 'priority', 'complexity',]