import json

from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.db.models import Q, Prefetch, Count
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.utils import timezone
from django.conf import settings
from django.views import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.detail import SingleObjectMixin
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _


from rest_framework import viewsets
from .serializers import TaskSerializer



from webapp.views import BaseContextMixin
from .models import Task
from .forms import TaskForm


class HierarchyRecursionCounter(object):

    """ Counter for hierarchy recursion """

    count = 0

    def increment(self):
        self.count += 1
        return ''

    def decrement(self):
        self.count -= 1
        return ''

    def double(self):
        self.count *= 2
        return ''

@method_decorator(login_required, name='dispatch')
class TasksListView(BaseContextMixin, ListView):

    """ Plain-list """
    
    model = Task
    template_name = 'tasks/task_plainlist.html'
    queryset = Task.objects.filter(parent__isnull=False).prefetch_related('parent') \
                    .annotate(tasks_count=Count('task')).filter(tasks_count=0)


class HierarchyViewMixin(object):

    def get_context_data(self, **kwargs):
        context = super(HierarchyViewMixin, self).get_context_data(**kwargs)
        context['recursion_counter'] = HierarchyRecursionCounter()
        return context


@method_decorator(login_required, name='dispatch')
class TasksListHierarchableView(BaseContextMixin, HierarchyViewMixin, ListView):

    """ Hierarchy """

    model = Task
    template_name = 'tasks/task_list_hierarchable.html'
    queryset = Task.objects.filter(parent=None).prefetch_related(Prefetch(
                'task_set',
                queryset=Task.objects.all().prefetch_related(Prefetch(
                            'task_set',
                            queryset=Task.objects.all().prefetch_related('task_set')\
                                        .annotate(subtasks_count=Count('task'))\
                                        .order_by('is_done', '-subtasks_count', '-priority', 'parent_id', '-created_at')
                )).annotate(subtasks_count=Count('task'))\
                .order_by('is_done', '-subtasks_count', '-priority', 'parent_id', '-created_at')
    )).annotate(subtasks_count=Count('task'))


@method_decorator(login_required, name='dispatch')
class TaskDetailView(BaseContextMixin, HierarchyViewMixin, DetailView):

    model = Task

    """ Prefetches several levels of child tasks """
    queryset = Task.objects.prefetch_related('parent', Prefetch(
                'task_set',
                queryset=Task.objects.all().prefetch_related(Prefetch(
                            'task_set',
                            queryset=Task.objects.all().prefetch_related('task_set')\
                                        .annotate(subtasks_count=Count('task'))\
                                        .order_by('is_done', '-subtasks_count', '-priority', 'parent_id', '-created_at')
                )).annotate(subtasks_count=Count('task'))\
                .order_by('is_done', '-subtasks_count', '-priority', 'parent_id', '-created_at')
    )).annotate(Count('task'))


@method_decorator(login_required, name='dispatch')
class TaskAddMixin(View):

    def get_context_data(self, **kwargs):
        context = super(TaskAddMixin, self).get_context_data(**kwargs)
        context['action'] = reverse('tasks:task-add') # form action url
        context['form_button_name'] = _('Add')
        return context


@method_decorator(login_required, name='dispatch')
class TaskUpdateMixin(SingleObjectMixin, View):

    def get_context_data(self, **kwargs):
        context = super(TaskUpdateMixin, self).get_context_data(**kwargs)
        context['action'] = reverse('tasks:task-update', args=[self.get_object().id,]) # form action url
        context['form_button_name'] = _('Update')
        return context


class TaskCreateUpdateDeleteMixin(object):

    model = Task
    
    def get_success_url(self):
        return self.request.POST['next_page']


class TaskCreateUpdateMixin(object):

    form_class = TaskForm


@method_decorator(login_required, name='dispatch')
class TaskCreate(BaseContextMixin, TaskCreateUpdateDeleteMixin, TaskCreateUpdateMixin, TaskAddMixin, CreateView):
    
    def form_valid(self, form):
        """ Assign user for task """
        form.instance.created_by = self.request.user
        return super(TaskCreate, self).form_valid(form)

    def get_initial(self):
        """ Sets parent task, if present in request """
        if self.request.method == 'GET' and self.kwargs.get('pk'):
            return {'parent': self.kwargs['pk']}


@method_decorator(login_required, name='dispatch')
class TaskUpdate(BaseContextMixin, TaskCreateUpdateDeleteMixin, TaskCreateUpdateMixin, TaskUpdateMixin, UpdateView):
    
    def get_action(self):
        """ Form action url """
        return reverse_lazy('tasks:task-update')


@method_decorator(login_required, name='dispatch')
class TaskToggleReady(View):
	
    """ Toggles ready state of the task """
    
    def get(self, request, pk):
        task = get_object_or_404(Task, pk=pk)
        task.toggle_ready()
        task.save()
        return redirect(self.request.META['HTTP_REFERER'])


@method_decorator(login_required, name='dispatch')
class TaskDelete(BaseContextMixin, TaskCreateUpdateDeleteMixin, DeleteView):
    
    pass


class TaskViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows tasks to be viewed or edited.
    """
    queryset = Task.objects.all().order_by('-created_at')
    serializer_class = TaskSerializer