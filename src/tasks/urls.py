from django.conf.urls import url, include
from . import views
from .views import TaskViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'all', TaskViewSet)

urlpatterns = [
    
    url(r'^$', views.TasksListView.as_view(), name='index'),

    url(r'^list/$', views.TasksListView.as_view(), name='task-list'),

    url(r'^hierarchable/$', views.TasksListHierarchableView.as_view(), name='task-list-hierarchable'),

    url(r'^add/$', views.TaskCreate.as_view(), name='task-add'),
    url(r'^add/(?P<pk>[0-9]+)/$', views.TaskCreate.as_view(), name='task-add-with-parent'),
    url(r'^update/(?P<pk>[0-9]+)/$', views.TaskUpdate.as_view(), name='task-update'),
    url(r'^update/(?P<pk>[0-9]+)/$', views.TaskUpdate.as_view(), name='task-update'),
    url(r'^delete/(?P<pk>[0-9]+)/$', views.TaskDelete.as_view(), name='task-delete'),
    url(r'^toggle-ready/(?P<pk>[0-9]+)/$', views.TaskToggleReady.as_view(), name='task-toggle-ready'),

    url(r'^task/(?P<slug>.+)/$', views.TaskDetailView.as_view(), name='task-detail'),
    url(r'^endpoints/', include(router.urls)),
]