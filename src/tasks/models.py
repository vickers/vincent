from django.contrib.auth.models import User
from django.urls import reverse
from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from datetime import datetime

# Create your models here.

import uuid

def unique_slugify(text, record_id, model):
    """ For slugs """
    counter = 0
    slug = slugify(text, allow_unicode=True)
    while True:
        try:
            matched = model.objects.get(slug=slug)
            if matched.id == record_id:
                break
            else:
                counter += 1
                slug = slugify('{}-{}'.format(text, counter), allow_unicode=True)
        except:
            break
    return slug


class Task(models.Model):
    
    """ Levels """
    LOW = 1
    MEDIUM = 2
    HIGH = 3
    
    EASY = 1
    REGULAR = 2
    HARDCORE = 3
    NIGHTMARE = 4
    
    """ Choices """
    PRIORITY_CHOICES = (
        (LOW, _('Low')),
        (MEDIUM, _('Medium')),
        (HIGH, _('High')),
    )

    COMPLEXITY_CHOICES = (
        (EASY, _('Easy')),
        (REGULAR, _('Regular')),
        (HARDCORE, _('Hardcore')),
        (NIGHTMARE, _('Nightmare')),
    )

    """ Fields """
    priority = models.IntegerField(
        choices=PRIORITY_CHOICES,
        default=LOW,
        verbose_name=_('Priority'),
    )

    complexity = models.IntegerField(
        choices=COMPLEXITY_CHOICES,
        default=REGULAR,
        verbose_name=_('Complexity'),
    )    

    name = models.CharField(max_length=255, verbose_name=_('Name'))
    description = models.TextField(max_length=255, null=True, blank=True, verbose_name=_('Description'))
    slug = models.SlugField(max_length=255, unique=True, allow_unicode=True, default=uuid.uuid4, verbose_name=_('Slug'))
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=_('Created by'))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('Created at'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('Updated at'))
    is_done = models.BooleanField(default=False, verbose_name=_('Is done'))
    done_at = models.DateTimeField(null=True, blank=True, verbose_name=_('Done at'))
    parent = models.ForeignKey('Task', null=True, blank=True, verbose_name=_('Parent'))
    deadline = models.DateTimeField(null=True, blank=True, verbose_name=_('Deadline at'))

    def __str__(self):
        return '{}'.format(self.name)
    
    def save(self, *args, **kwargs):
        """ Generates slug each save """
        self.slug = unique_slugify(self.name, self.id, Task)
        self.set_done_at_if_done()
        super(Task, self).save(*args, **kwargs)

    def get_absolute_url(self):
       return reverse('tasks:task-detail', kwargs={'slug': self.slug})

    def set_done_at_if_done(self):
        if self.is_done:
            self.done_at = datetime.now()
        else:
            self.done_at = None

    def toggle_ready(self):
        self.is_done = False if self.is_done else True
        self.set_done_at_if_done()

    class Meta:
        verbose_name = _('Task')
        verbose_name_plural = _('Tasks')
        ordering = ['is_done', '-priority', 'parent_id', '-created_at',]
