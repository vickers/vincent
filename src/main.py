import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "vincent.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from shop.models import Product

products = Product.objects.prefetch_related('category').all()

for p in products:
    print('{}, {} - ${}'.format(p.category.name, p.name, p.price))

from django.db import connection
print(len(connection.queries))




# SELECT  DISTINCT ON (phones.phone)
#         phones.phone, 
#         COUNT(DISTINCT items1.id) as count_sold,
#         COUNT(DISTINCT items2.id) as count_not_sold
# FROM phones
# LEFT JOIN items AS items1
# ON items1.user_id=ANY(phones.users) AND items1.status=7
# LEFT JOIN items AS items2
# ON items2.user_id=ANY(phones.users) AND items2.status=3
# WHERE phones.phone=ANY(array['1234234234', '234234234'])
# GROUP BY phones.phone;



# SELECT  DISTINCT ON (phones.phone)
#         phones.phone, 
#         count(items2.id) as count_not_sold,
#         array_agg(items2.id),
#         array_agg(phones.phone)
# FROM phones
# INNER JOIN items AS items2
# ON items2.user_id=ANY(phones.users) AND items2.status=3
# WHERE phones.phone=ANY(array['1234234234', '234234234'])
# GROUP BY phones.phone;




# SELECT  DISTINCT ON (phones.id)
#         phones.phone, 
#         count(items1.*) as count_sold, 
#         array_agg(items1.id),
#         array_agg(phones.phone)
# FROM phones
# LEFT JOIN items AS items1
# ON items1.user_id=ANY(phones.users) AND items1.status=7
# WHERE phones.phone=ANY(array['1234234234', '234234234'])
# GROUP BY phones.phone;

