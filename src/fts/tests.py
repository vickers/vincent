from django.test import LiveServerTestCase
from django.test import TestCase
from django.urls import reverse
from selenium import webdriver

# Create your tests here.

class URLMaker(object):
    
    def make_url(self, path):
        return '{}{}'.format(self.live_server_url, path)


class VincentTest(LiveServerTestCase, URLMaker):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_create_new_poll_via_admin_site(self):
        # Vincent opens his web browser, and goes to the admin page
        self.browser.get(self.make_url(reverse('admin:login')))

        # He sees the familiar 'Django administration' heading
        body = self.browser.find_element_by_tag_name('body')
        self.assertIn('Django administration', body.text)

        # Goes to index page
        self.browser.get(self.make_url(reverse('index')))

        # Sees "Welcome"
        body = self.browser.find_element_by_tag_name('body')        
        self.assertIn('Welcome', body.text)


