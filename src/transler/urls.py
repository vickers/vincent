from django.conf.urls import url
from . import views


urlpatterns = [
    
    url(r'^$', views.WordsListView.as_view(), name='index'),

]