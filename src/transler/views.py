import json

# from django.shortcuts import render_to_response, get_object_or_404
# from django.contrib.auth.decorators import login_required
# from django.utils.decorators import method_decorator
# from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.db.models import Prefetch#, Count, Q
# from django.http import HttpResponse
# from django.shortcuts import render, redirect
# from django.utils import timezone
from django.conf import settings
# from django.views import View
# from django.views.generic.edit import CreateView, UpdateView, DeleteView
# from django.urls import reverse, reverse_lazy
# from django.utils.translation import ugettext_lazy as _

# from django.views.generic.detail import SingleObjectMixin

from webapp.views import BaseContextMixin
from .models import Word, Language
# from .forms import TaskForm



# Create your views here.

class WordsListView(BaseContextMixin, ListView):

    model = Word
    queryset = Word.objects.prefetch_related(Prefetch(
        'words',
        queryset=Word.objects.prefetch_related('language').order_by('-frequency')
    )).prefetch_related('language')

