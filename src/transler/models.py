from django.db import models
from django.utils import timezone

# Create your models here.

class BaseModel(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(default=timezone.now)

class Language(BaseModel):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class Word(BaseModel):
    name = models.CharField(max_length=30)
    language = models.ForeignKey(Language, blank=False)
    words = models.ManyToManyField("self", symmetrical=False, blank=True)
    frequency = models.IntegerField(default=0)

    def __str__(self):
        return self.name