from django.contrib import admin
from .models import Category, Product, Value, Attribute, Discount, Brend

# Register your models here.


admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Brend)
admin.site.register(Value)
admin.site.register(Attribute)
admin.site.register(Discount)
