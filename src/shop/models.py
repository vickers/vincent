from django.db import models

# Create your models here.




class ProductManager(models.Manager):

    def with_actual_price(self, price_low=-1, price_high=1000000):
        

        return self.raw('''
            SELECT *
            FROM (
                SELECT  *, CASE WHEN discount=0 THEN price ELSE price-(price*(discount/100)) END AS act_price
                FROM (
                    SELECT  
                        DISTINCT ON(sp.id)
                        sp.id AS id, 
                        sp.name AS name,
                        sp.price AS price,
                        COALESCE((
                            SELECT MAX(discounts.percent) 
                            FROM (  SELECT MAX(sd_c.percent) as percent UNION
                                    SELECT MAX(sd_p.percent) as percent UNION
                                    SELECT MAX(sd_b.percent) as percent UNION
                                    SELECT MAX(sd_v.percent) as percent ) discounts
                        ), 0) AS discount
                    FROM shop_product as sp
                    LEFT JOIN shop_category as sc
                    ON sp.category_id=sc.id

                    -- category discounts                    
                    LEFT JOIN shop_discount_category as sd_c1
                    ON sd_c1.category_id = sc.id
                    LEFT JOIN shop_discount as sd_c
                    ON sd_c.id = sd_c1.discount_id
                    AND sd_c.date_start < now() AND sd_c.date_end > now()
                    
                    -- product discounts
                    LEFT JOIN shop_discount_product as sd_p1
                    ON sd_p1.product_id = sp.id 
                    LEFT JOIN shop_discount as sd_p
                    ON sd_p.id = sd_p1.discount_id
                    AND sd_p.date_start < now() AND sd_p.date_end > now()

                    -- brend discounts
                    LEFT JOIN shop_discount_brend as sd_b1
                    ON sd_b1.brend_id = sp.brend_id 
                    LEFT JOIN shop_discount as sd_b
                    ON sd_b.id = sd_b1.discount_id 
                    AND sd_b.date_start < now() AND sd_b.date_end > now()

                    -- value discounts
                    LEFT JOIN shop_value_products as svp
                    ON svp.product_id=sp.id
                    LEFT JOIN shop_discount_value as sd_v1
                    ON sd_v1.value_id = svp.value_id
                    LEFT JOIN shop_discount as sd_v
                    ON sd_v.id = sd_v1.discount_id 
                    AND sd_v.date_start < now() AND sd_v.date_end > now()

                    GROUP BY sp.id
                ) t
                ORDER BY act_price
            ) t1
            WHERE act_price>%s AND act_price<%s;

            ''', [price_low, price_high,])


class Category(models.Model):
    name = models.CharField(verbose_name="Группа товара", max_length=64)

    def __str__(self):
        return self.name


class Brend(models.Model):
    name = models.CharField(verbose_name="Название бренда", max_length=128)

    def __str__(self):
        return self.name


class Product(models.Model):
    category = models.ForeignKey(Category, verbose_name="Группа", related_name="products")
    name = models.CharField(verbose_name="Название товара", max_length=128)
    brend = models.ForeignKey(Brend, blank=True, null=True)
    price = models.DecimalField(verbose_name="Стоимость единицы, руб.", max_digits=10, decimal_places=2)
    objects = ProductManager()

    def __str__(self):
        return self.name


class Attribute(models.Model):
    name = models.CharField(max_length=128)
    categories = models.ManyToManyField(Category, related_name="attributes")

    def __str__(self):
        return self.name


class Value(models.Model):
    name = models.CharField(max_length=64)
    attribute = models.ForeignKey(Attribute)
    products = models.ManyToManyField(Product, blank=True, related_name="values")

    def __str__(self):
        return '{}: {}'.format(self.attribute.name, self.name)


class Discount(models.Model):
    name = models.CharField(max_length=64)
    percent = models.FloatField()
    date_start = models.DateTimeField(blank=True, null=True)
    date_end = models.DateTimeField(blank=True, null=True)
    category = models.ManyToManyField(Category, blank=True)
    product = models.ManyToManyField(Product, blank=True)
    brend = models.ManyToManyField(Brend, blank=True)
    value = models.ManyToManyField(Value, blank=True)

    def __str__(self):
        return '{} - {}%'.format(self.name, self.percent)
