# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-02-03 21:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0006_auto_20170203_2113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discount',
            name='attribute',
            field=models.ManyToManyField(blank=True, to='shop.Attribute'),
        ),
        migrations.AlterField(
            model_name='discount',
            name='category',
            field=models.ManyToManyField(blank=True, to='shop.Category'),
        ),
        migrations.AlterField(
            model_name='discount',
            name='product',
            field=models.ManyToManyField(blank=True, to='shop.Product'),
        ),
    ]
