# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2017-02-04 08:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0007_auto_20170203_2114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discount',
            name='attribute',
            field=models.ManyToManyField(blank=True, to='shop.Value'),
        ),
    ]
