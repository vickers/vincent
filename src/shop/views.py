from django.shortcuts import render
from django.http import HttpResponse
from .models import Product
from webapp.views import BaseContextMixin
from django.views.generic.list import ListView

# Create your views here.
def index(request):
    products = Product.objects.all()
    response = ', <br>'.join(["{} - ${}".format(p.name, p.price) for p in products])
    return HttpResponse(response)





class ProductListView(BaseContextMixin, ListView):

    """ Plain-list """
    
    template_name = 'shop/index.html'
    queryset = Product.objects.with_actual_price()
