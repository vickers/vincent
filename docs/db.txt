sudo su postgres
psql
CREATE DATABASE vincent;
CREATE ROLE vincent WITH PASSWORD 'vincent';
GRANT ALL privileges ON DATABASE vincent TO vincent;
ALTER ROLE "vincent" WITH login;
ALTER USER username CREATEDB; --- tests create their own database